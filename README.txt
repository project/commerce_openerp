Commerce OpenERP Integration

This module provides an integration between Drupal Commerce and OpenERP. Orders,
products, product categories and customers are automatically synced from
Drupal Commerce to OpenERP.

Currently, the module assumes that products are created and edited in Drupal
Commerce (and not in OpenERP). There is support, though, to sync price and stock
level changes from OpenERP to Drupal Commerce.

Installation
------------

1. Install the OpenERP API module (http://drupal.org/project/openerp).

2. Apply the patch from https://drupal.org/node/2175479 to the openerp module.

3. Install and enable the OpenERP API module and configure the connection to
   an OpenERP database (see OpenERP module documentation).

3. Install and enable Commerce OpenERP.

4. Go to admin/commerce/config/openerp to configure Commerce OpenERP. Read the
   features section below for details.

5. Run cron or click the sync button on the config page or create orders in
   Drupal Commerce. You should see orders, customers and products appear in
   OpenERP.

Features
--------

Most features can be enabled or disabled on the module's configuration page.

* Orders are created in OpenERP as soon as the "Order complete" page is reached
  in Drupal Commerce's checkout process. The included products and customer
  profiles are synced as well.

* To copy product images to OpenERP, install the image_base64 module, and resync
  the products. You can configure an image style to be used for the images that
  OpenERP receives. NOTE: If you're not using Commerce Kickstart, create an
  image field named "field_images" on your product variations.

* Product prices and, if the Commerce Stock module [2] is enabled, stock levels
  can be synced from OpenERP to Commerce.

* Order status changes can be synced from OpenERP to Commerce. Currently the
  only order status that is synced is the "done" state in OpenERP, which makes
  orders in Drupal Commerce be "completed".

* Product categories are synced as well, including hierarchies. NOTE: If you're
  not using Commerce Kickstart, create a taxonomy term reference field named
  "field_product_category" on your product display node types.

* Basic support for mapping taxes and handling discounts is also available.
  Make sure to configure the mapping of tax rates on the module's configuration
  page. Also make sure that the mapped tax rates are both configured either
  inclusive or non-inclusive, otherwise you will get wrong order totals in OpenERP.


Known issues
------------

* The "Sync now" and "Resync everything" buttons on the module's config page
  should be implemented via the Batch API, but are not (yet) - so they will
  likely time out if you have many products. For now, just use cron via drush
  instead.

* Drupal Commerce creates new commerce_customer_profile entities for each order,
  even if the address information is identical to a previous order by the same
  user. This means that a new client resource is created in OpenERP as well. To
  somehow manage this non-ideal situation (having many identical clients in
  OpenERP), the first customer profile of each Drupal user is created in OpenERP
  as a company, because in OpenERP, only companies can have sub-contacts
  (regular clients cannot). The addresses created for subsequent orders are
  created as sub-contacts of the first client. Subsequent orders are also
  assigned to the master client. The matching is done by email address, so if
  you have existing clients in your OpenERP database with the same email address
  as someone who does an order via Commerce, the order will be attributed to the
  existing client in OpenERP (but with the correct address coming from
  Commerce).

API
---

The main API consists of a single class, CommerceOpenErpIntegration. It works on
metadata-wrapped Drupal entities. The information to map Drupal entities to
OpenERP models is not stored in Drupal, but in OpenERP, in the "External ID"
field. There are API methods availble to easily receive the mappings in both
directions.

TODO
----

* Write tests.
* Implement batch API for the sync buttons on the configuration page.
* Add better support for order states.
* Add support to track payments in both directions (payment received in
  Commerce: mark invoice payed in OpenERP and vice versa).
* Add support to import products from OpenERP to Commerce.
